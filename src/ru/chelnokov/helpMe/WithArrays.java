package ru.chelnokov.helpMe;

import java.util.Arrays;

/**
 * Класс-задачка, который выводит отдельно из исходного массива
 * числа, кратные трём/пяти/и трём, и пяти
 *
 * Но сделанная с помощью массивов вместо ArrayList
 *
 * @author Chelnokov E.I., 16IT18K
 */

public class WithArrays {
    public static void main(String[] args) {
        Integer[] array = {3, 5, 8, 4, 67, 43, 3, 6, 2, 8, 84};//исходный массив
        int a = 0;//это переменные, отличающие за длину массивов с результатами. Будь с ними аккуратнее
        int b = 0;
        int c = 0;
        int[] dividedOnThree = new int[a];//Массивы, в которые мы рассортируем числа. Пока пустые.
        int[] dividedOnFive = new int[b];
        int[] dividedOnThreeAndFive = new int[c];
        for (int i = 0;i< array.length;i++) {
            if (array[i] % 3 == 0) {
                a = a + 1;//увеличиваем длину массива
                dividedOnThree = Arrays.copyOf(dividedOnThree, a);//создаём новый массив с новой длиной, но теми же данными
                dividedOnThree[a - 1] = array[i];

            }
            if (array[i] % 5 == 0) {
                b = b + 1;
                dividedOnFive = Arrays.copyOf(dividedOnFive, b);
                dividedOnFive[b - 1] = array[i];

            }
            if ((array[i] % 3 == 0) && (array[i] % 5 == 0)) {
                c = c + 1;
                dividedOnThreeAndFive = Arrays.copyOf(dividedOnThreeAndFive, c);
                dividedOnThreeAndFive[c - 1] = array[i];

            }
        }
        String dividedNumbers = "Числа, которые кратны ";
        System.out.print(dividedNumbers + "трём - ");
        correctOutput(dividedOnThree);
        System.out.print(dividedNumbers + "пяти - ");
        correctOutput(dividedOnFive);
        System.out.print(dividedNumbers + "и трём, и пяти - ");
        correctOutput(dividedOnThreeAndFive);
    }

    /**
     * Метод для корректного вывода результата
     * @param array массив с кратными числами
     */
    private static void correctOutput(int[] array) {
        String isEmpty = "Извините, таких чисел нет";
        if (array.length == 0) {
            System.out.println(isEmpty);
        } else {
            System.out.println(Arrays.toString(array));
        }
    }
}


