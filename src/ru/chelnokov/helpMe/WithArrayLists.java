package ru.chelnokov.helpMe;

import java.util.ArrayList;

/**
 * Класс-задачка, который выводит отдельно из исходного массива
 * числа, кратные трём/пяти/и трём, и пяти
 *
 * @author Chelnokov E.I., 16IT18K
 */
public class WithArrayLists {
    public static void main(String[] args) {
        Integer[] array = {3, 5, 8, 4, 67, 43, 3, 6, 2, 8, 84};
        ArrayList<Integer> dividedOnThreeAndFive = new ArrayList<>();
        ArrayList<Integer> dividedOnThree = new ArrayList<>();
        ArrayList<Integer> dividedOnFive = new ArrayList<>();
        for (int i = 0; i <array.length;i++ ) {
            if (array[i] % 3 == 0) {
                dividedOnThree.add(array[i]);
            }
            if (array[i] % 5 == 0) {
                dividedOnFive.add(array[i]);
            }
            if ((array[i] % 3 == 0) && (array[i] % 5 == 0)) {
                dividedOnThreeAndFive.add(array[i]);
            }
        }

        String dividedNumbers = "Числа, которые кратны ";
        System.out.print(dividedNumbers + "трём - ");
        correctOutput(dividedOnThree);
        System.out.print(dividedNumbers + "пяти - ");
        correctOutput(dividedOnFive);
        System.out.print(dividedNumbers + "и трём, и пяти - ");
        correctOutput(dividedOnThreeAndFive);
    }

    /**
     * Метод для корректного вывода результата
     * @param arrayList массив с кратными числами
     */
    private static void correctOutput(ArrayList<Integer> arrayList) {
        String isEmpty = "Извините, таких чисел нет";
        if (arrayList.isEmpty()){
            System.out.println(isEmpty);
        }else{
            System.out.println(arrayList.toString());
        }
    }
}
